import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class StartButtonAction implements ActionListener{
	GameStartPanel startPanel;
	StartButtonAction(GameStartPanel s){
		startPanel = s;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		startPanel.frame.z.J.setName(startPanel.t1.getText());
		System.out.println(startPanel.t1.getText());
		startPanel.frame.l1.setText("Name: "+ startPanel.frame.z.J.getName());
		startPanel.frame.z.J.setSex(startPanel.t2.getText());
		System.out.println(startPanel.t2.getText());
		startPanel.frame.l2.setText("Gender: "+ startPanel.frame.z.J.getSex());
		startPanel.frame.z.J.setAge(Integer.parseInt(startPanel.t3.getText()));
		System.out.println(startPanel.t3.getText());
		startPanel.frame.l3.setText("Age: "+ startPanel.frame.z.J.getAge());
		startPanel.setVisible(false);
		startPanel.frame.mapPanel.setVisible(true);
	}
	
}
