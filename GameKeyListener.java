import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

//import game.GameFrame;

public class GameKeyListener implements KeyListener {
	GameFrame frame;
	GameKeyListener(GameFrame f) {
		frame = f;
	}
	public void keyTyped(KeyEvent e) {
		
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int a = e.getKeyCode();
		char c;
		switch (a) {
			case 65: c = 'a'; break;
			case 87: c = 'w'; break;
			case 68: c = 'd'; break;
			case 83: c = 's'; break;
			default: c = '/';
		}
		frame.z.J.move(c,frame.z);
//		frame.repaint();
//		System.out.println(a);

		frame.mapPanel.repaint();

		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

}
