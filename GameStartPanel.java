import java.awt.Button;
import java.awt.Color;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;

public class GameStartPanel extends Panel {
	GameFrame frame;
	Label l1;
	Label l2;
	Label l3;
	TextField t1;
	TextField t2;
	TextField t3;
	Button b;
	
	public GameStartPanel(GameFrame f) {
		frame = f;
		f.setBackground(Color.lightGray);
		l1 = new Label("Choose your name");
		l1.setLocation(380, 100);
		l1.setSize(250, 30);
		this.add(l1);
		l2 = new Label("Male or Female");
		l2.setLocation(380, 250);
		l2.setSize(250, 30);
		this.add(l2);
		l3 = new Label("Enter your age");
		l3.setLocation(380, 400);
		l3.setSize(250, 30);
		this.add(l3);
		t1 = new TextField();
		t1.setLocation(310, 150);
		t1.setSize(250, 30);
		this.add(t1);
		t2 = new TextField();
		t2.setLocation(310, 300);
		t2.setSize(250, 30);
		this.add(t2);
		t3 = new TextField();
		t3.setLocation(310, 450);
		t3.setSize(250, 30);
		this.add(t3);
		b = new Button("Start");
		b.setLocation(380, 520);
		b.setSize(100, 30);
		StartButtonAction sba = new StartButtonAction(this);
		b.addActionListener(sba);
		this.add(b);
		this.setLayout(null);
		}

}
