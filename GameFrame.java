import java.awt.Font;
import java.awt.Frame;
import java.awt.Label;

public class GameFrame extends Frame {
	map z;
	GameMapPanel mapPanel;
	GameStartPanel startPanel;
	Label l1;
	Label l2;
	Label l3;
	Label l4;
	GameFrame(int x, int y, map m){
		z = m;
		setSize(x, y);
		setVisible(true);
		GameKeyListener gkl = new GameKeyListener(this);
		addKeyListener(gkl);
		GameWindowListener gwl = new GameWindowListener();
		addWindowListener(gwl);
		mapPanel = new GameMapPanel(z, this);
		mapPanel.setLocation(50, 70);
		mapPanel.setSize(z.sizex * 30 + 1, z.sizey * 30 + 1);
		mapPanel.addKeyListener(gkl);
		this.add(mapPanel);
		startPanel = new GameStartPanel(this);
		startPanel.setLocation(0, 0);
		startPanel.setSize(this.getWidth(),this.getHeight());
		this.add(startPanel);
		l1 = new Label("Name: " + z.J.getName());
		l1.setFont(new Font("Normal", Font.PLAIN, 30));
		l1.setLocation(600, 100);
		l1.setSize(250, 30);
		this.add(l1);
		l2 = new Label("Gender: " + z.J.getSex());
		l2.setFont(new Font("Normal", Font.PLAIN, 30));
		l2.setLocation(600, 150);
		l2.setSize(250, 30);
		this.add(l2);
		l3 = new Label("Age: " + z.J.getAge()); // big brain time!
		l3.setFont(new Font("Normal", Font.PLAIN, 30));
		l3.setLocation(600, 200);
		l3.setSize(250, 40);
		this.add(l3);
		l4 = new Label();
		l4.setFont(new Font("Normal", Font.PLAIN, 30));
		l4.setLocation(180, 600);
		l4.setSize(620, 40);
		this.add(l4);
		this.setLayout(null);

	}
	
}
